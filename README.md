All you need to use:
1) optional: an application.properties file with such properties:
    jwt.secret (default: test)
    jwt.expiration.seconds (default: 1 day)
    
2) optional: a class implementing com.gemicle.spring.reactive.security.config.SecurityConfigurer interface
default: all endpoints are secured
