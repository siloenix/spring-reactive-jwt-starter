package com.gemicle.spring.reactive.security.config;

import org.springframework.security.config.web.server.ServerHttpSecurity;

public interface SecurityConfigurer {
    void configure(final ServerHttpSecurity http);
}
