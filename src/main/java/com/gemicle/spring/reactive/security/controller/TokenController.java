package com.gemicle.spring.reactive.security.controller;

import com.gemicle.spring.reactive.security.auth.JwtAuthenticationRequest;
import com.gemicle.spring.reactive.security.auth.JwtAuthenticationResponse;
import com.gemicle.spring.reactive.security.auth.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(path = "/auth", produces = {APPLICATION_JSON_UTF8_VALUE})
public class TokenController {
    @Autowired
    private ReactiveUserDetailsService service;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;



    @RequestMapping(method = POST, value = "/token")
    @CrossOrigin("*")
    public Mono<ResponseEntity<JwtAuthenticationResponse>> token(@RequestBody JwtAuthenticationRequest authenticationRequest) throws AuthenticationException {
        String username = authenticationRequest.getUsername();
        String password = authenticationRequest.getPassword();
        System.out.println(username);
        System.out.println(password);
        return service.findByUsername(authenticationRequest.getUsername())
                .map(user -> ok().contentType(APPLICATION_JSON_UTF8).body(
                        new JwtAuthenticationResponse(jwtTokenUtil.generateToken(user), user.getUsername()))
                )
                .defaultIfEmpty(notFound().build());
    }
}
